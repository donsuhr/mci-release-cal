module.exports = function scsslint(grunt, options) {
    'use strict';
    return {
        allFiles: [
            `${options.app}/styles/**/*.scss`,
        ],
        options: {
            bundleExec: false,
            config: '.scss-lint.yml',
            reporterOutput: null, // '.tmp/scss-lint-report.xml',
            colorizeOutput: true,
            exclude: [`${options.app}/styles/vendor/**`],
        },
    };
};
