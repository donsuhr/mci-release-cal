module.exports = function cssmin(grunt, options) {
    return {
        dist: {
            files: [
                {
                    expand: true,
                    cwd: '.tmp/styles',
                    src: ['**/*.css'],
                    dest: `${options.dist}/styles/`,
                    ext: '.css',
                },
            ],
        },
    };
};
