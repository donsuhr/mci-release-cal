module.exports = function watch(grunt, options) {
    return {
        js: {
            files: [
                './**/*.js',
                '!./app/scripts/vendor/**',
                '!./node_modules/**',
                '!./dist/**',
                '!./.tmp/**',
            ],
            tasks: ['eslint'],
            options: {
                // js reloading by webpack hot module
                livereload: false,
            },
        },
        sass: {
            files: ['./app/styles/**/*.scss'],
            tasks: ['sass', 'autoprefixer'],
        },
        sassWithLint: {
            files: ['./app/styles/**/*.scss'],
            tasks: ['sass', 'autoprefixer', 'scsslint'],
        },
        livereload: {
            options: {
                livereload: options.livereloadPort,
            },
            files: [
                '.tmp/styles/**/*.css',
                '.tmp/{,*/}*.html',
                `${options.app}/**/*.html`,
                `${options.app}/images/{,*/}*`,
            ],
        },
    };
};

