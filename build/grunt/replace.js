module.exports = function replace(grunt, options) {
    return {
        cssImgOne: {
            src: [`${options.dist}/styles/**/*.css`],
            overwrite: true,
            replacements: [
                {
                    from: '\/images\/',
                    to: '/SiteCollectionImages/',
                },
            ],
        },
    };
};
