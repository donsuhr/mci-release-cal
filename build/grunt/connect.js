const serveStatic = require('serve-static');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const webpackConfig = require('../../webpack.config');
const bundler = webpack(webpackConfig);

module.exports = function gruntConnect(grunt, options) {
    return {
        options: {
            port: 9003,
            open: true,
            livereload: options.livereloadPort,
            // Change this to '0.0.0.0' to access the server from outside
            hostname: '*',
        },
        livereload: {
            options: {
                middleware(connect, middlewareOptions) {
                    return [
                        webpackDevMiddleware(bundler, {
                            // IMPORTANT: dev middleware can't access config, so we should
                            // provide publicPath by ourselves
                            publicPath: webpackConfig.output.publicPath,
                            stats: {
                                colors: true,
                            },
                        }),
                        // bundler should be the same as above
                        webpackHotMiddleware(bundler),
                        function postToGet(req, res, next) {
                            if (req.method === 'POST') {
                                // eslint-disable-next-line no-param-reassign
                                req.method = 'GET';
                            }
                            return next();
                        },
                        serveStatic('.tmp'),
                        // connect().use('/bower_components', serveStatic('./bower_components')),
                        connect().use('/SiteCollectionImages', serveStatic('./app/images')),
                        serveStatic(options.app),
                    ];
                },
            },
        },
        dist: {
            options: {
                port: 9002,
                base: options.dist,
                livereload: false,
            },
        },
    };
};
