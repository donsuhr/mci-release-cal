module.exports = function sass(grunt, options) {
    return {
        options: {
            sourceMap: true,
        },
        dev: {
            files: [{
                expand: true,
                cwd: 'app/styles',
                src: ['**/*.scss'],
                dest: options.sassOutput,
                ext: '.css',
            }],
        },
    };
};
