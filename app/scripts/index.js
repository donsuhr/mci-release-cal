import _ from 'lodash';

// using globaly/masterpage loaded jquery and spservices
const pageJquery = window.jQuery;
const $ = require('jquery');

// require('babel-polyfill');
const moment = require('moment');
const loadingTemplate = require('templates/loadingTemplate.hbs');
const monthsTemplate = require('templates/months.hbs');
const warningTemplate = require('templates/warning.hbs');
const versionCompare = require('node-version-compare');

// npm install --save node-version-compare
// recycle window jQuery -----------------------
window.jQuery = window.$ = $;

require('bower/SPServices/src/jquery.SPServices.js');
require('bootstrap-sass/assets/javascripts/bootstrap/alert');
require('bootstrap-sass/assets/javascripts/bootstrap/modal');

if (process.env.NODE_ENV !== 'production') {
    require('./mockjax-config');
}

window.jQuery = window.$ = pageJquery;
// end recycle window jQuery -----------------------

const defaults = {
    showWarning: true,
    webURL: '/Releases/',
    listName: 'Releases - v2',
};
let _options = {};

const _el = document.getElementById('releaseCalApp');
const _$el = $(_el);
let _$loadingEl;

const start = moment().date(1);
const end = start.clone().add(12, 'months');

function hideLoading() {
    if (_$loadingEl && _$loadingEl.length) {
        _$loadingEl.remove();
    }
}

function showLoading() {
    hideLoading();
    _$loadingEl = $(loadingTemplate());
    _$el.append(_$loadingEl);
    const x = (_$el.innerWidth() / 2 - _$loadingEl.innerWidth() / 2);
    const y = Math.max(0, (_$el.innerHeight() / 2 - _$loadingEl.innerHeight() / 2));
    _$loadingEl.css({ left: x, top: y });
}

function onDocumentClick(event) {
    if ($(event.target).is('.release-cal-details-link')) {
        event.preventDefault();
    }
}

function resetListeners() {
    document.addEventListener('click', onDocumentClick);
}

function renderClear() {
    _el.innerHTML = '';
}

function render(data) {
    const totalColumns = 2 + end.diff(start, 'months') + 1;
    const html = monthsTemplate({
        start,
        end,
        data,
        totalColumns,
    });
    hideLoading();
    _el.innerHTML = html;
    resetListeners();
}

function loadData() {
    renderClear();
    showLoading();
    return new Promise((resolve, reject) => {
        if (_options.seedData && _options.seedData.length) {
            resolve(_options.seedData);
        } else {
            // eslint-disable-next-line new-cap
            const getItemsJqueryPromise = $().SPServices.SPGetListItemsJson({
                webURL: _options.webURL,
                listName: _options.listName,
                CAMLQuery: `<Query><Where>
                   <And>
                      <Eq><FieldRef Name="Display"/><Value Type="Integer">1</Value></Eq>
                      <And>
                        <Geq>
                            <FieldRef Name="Estimated_x0020_Release_x0020_Da"/>
                            <Value Type="DateTime">${start.toISOString()}</Value>
                        </Geq>
                        <Leq>
                            <FieldRef Name="Estimated_x0020_Release_x0020_Da"/>
                            <Value Type="DateTime">${end.toISOString()}</Value>
                        </Leq>
                      </And>
                   </And>
                </Where></Query>`,
                CAMLViewFields: `<ViewFields>
                    <FieldRef Name="Product_x0020_Sub_x0020_Category"/>
                    <FieldRef Name="Release_x0020_Version"/>
                    <FieldRef Name="Estimated_x0020_Release_x0020_Da"/>
                    <FieldRef Name="Product"/>
                    <FieldRef Name="Display"/>
                    <FieldRef Name="Details"/>
                </ViewFields>`,
                debug: false,
            });

            $.when(getItemsJqueryPromise).done(function cb() {
                resolve(this.data);
            });
        }
    });
}

function onProceedClicked() {
    loadData().then(result => {
        const withinRange = _.filter(result,
            (item) => moment(item.Estimated_x0020_Release_x0020_Da).isBetween(start, end)
        );
        const sortedResult = _.sortBy(withinRange, 'Product');
        const byProduct = _.groupBy(sortedResult, 'Product');
        _.forEach(byProduct, (product, key, collection) => {
            // eslint-disable-next-line no-param-reassign
            collection[key] = _.groupBy(product, 'Product_x0020_Sub_x0020_Category');
            _.forEach(collection[key], val => {
                // eslint-disable-next-line max-len
                val.sort((a, b) => versionCompare(a.Release_x0020_Version, b.Release_x0020_Version));
            });
        });
        render(byProduct);
    });
}

function renderWarning() {
    _el.innerHTML = warningTemplate();
    $().alert();
    $('.alert .btn-primary').on('click', onProceedClicked);
}

module.exports = {
    init(options) {
        _options = _.assign({}, defaults, options);
        if (_options.showWarning) {
            renderWarning();
        } else {
            onProceedClicked();
        }
    },
};

