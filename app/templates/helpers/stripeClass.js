export default function stripeClass(index, options) {
    return (index % 2 === 0) ?
        'release-cal-table__data-row--even' :
        'release-cal-table__data-row--odd';
}
