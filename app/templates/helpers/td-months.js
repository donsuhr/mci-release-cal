import moment from 'moment';

export default function tdMonths(start, end, date) {
    const counter = start.clone();
    const ret = [];
    const releaseDate = moment(date);
    while (counter <= end) {
        const sameYear = counter.year() === releaseDate.year();
        const sameMonth = counter.month() === releaseDate.month();
        const content = sameYear && sameMonth ?
            '<span class="release-cal-table__td--checked">checked</span>' :
            '&nbsp;';
        ret.push(`<td>${content}</td>`);
        counter.add(1, 'month');
    }
    return ret.join('\n');
}
