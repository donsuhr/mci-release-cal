export default function totalColumns(start, end) {
    return 2 + end.diff(start, 'months');
}
