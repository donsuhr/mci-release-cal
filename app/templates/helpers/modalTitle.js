export default function modalTitle(firstChoice, secondChoice, options) {
    if (firstChoice !== 'undefined') {
        return `${firstChoice} - `;
    } else if (secondChoice !== 'undefined') {
        return `${secondChoice} - `;
        // eslint-disable-next-line no-else-return
    } else {
        return '';
    }
}
