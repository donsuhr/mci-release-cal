export default function thMonths(start, end) {
    const counter = start.clone();
    const ret = [];

    while (counter <= end) {
        const cssClass = counter.month() === 11 ? 'release-cal-table__th--month--last' : '';
        ret.push(`<th scope="col" class="${cssClass}">${counter.format('MMM')}</th>`);
        counter.add(1, 'month');
    }

    return ret.join('\n');
}
