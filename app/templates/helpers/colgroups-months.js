export default function colgroupsMonths(start, end) {
    const counter = start.clone();
    const ret = [];

    while (counter <= end) {
        const lastMonth = end.year() === counter.year() ? end.month() : 11;
        const startMonth = counter.year() === start.year() ? counter.month() : 0;
        const remaining = lastMonth - startMonth + 1;
        let colGroup = `<colgroup class="release-cal-table__colgroup--year" span="${remaining}">`;
        let colCounter = startMonth;
        while (colCounter <= lastMonth) {
            colGroup += '<col class="release-cal-table__col--year">\n';
            colCounter++;
        }

        colGroup += '</colgroup>';
        ret.push(colGroup);

        if (end.year() === counter.year()) {
            break;
        } else {
            counter.add(1, 'year').month(0);
        }
    }

    return ret.join('\n');
}
