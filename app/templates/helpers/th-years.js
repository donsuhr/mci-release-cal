export default function thYears(start, end) {
    const counter = start.clone();
    const ret = [];

    while (counter <= end) {
        const lastMonth = end.year() === counter.year() ? end.month() : 11;
        const startMonth = counter.year() === start.year() ? counter.month() : 0;
        const remaining = lastMonth - startMonth + 1;
        ret.push(`<th scope="colgroup" colspan="${remaining}">${counter.format('YYYY')}</th>`);
        if (end.year() === counter.year()) {
            break;
        } else {
            counter.add(1, 'year').month(0);
        }
    }

    return ret.join('\n');
}
